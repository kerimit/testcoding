<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class Register extends Controller
{
    public function index()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        $request->validate([
            'username' => ['required','unique:users'],
            'password' => ['required'],
            'repassword' => ['required'],
            'name' => ['required'],
            'email' => 'required|email|unique:users',
            'telp' => ['required'],
            'sim' => ['required'],
            'address' => ['required'],
        ]);
        $data = $request->all();
        $success = User::create([
            'username'=>$data['username'],
            'password' => Hash::make($data['password']),
            'name' => $data['name'],
            'email'=>$data['email'],
            'name'=>$data['name'],
            'telp'=>$data['telp'],
            'sim'=>$data['sim'],
            'address'=>$data['address']
        ]);
        if($success) return redirect("/")->withSuccess('Registrasi berhasil. Silahkan Sign in...');
        return redirect('/register');
    }
}
