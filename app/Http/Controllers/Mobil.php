<?php
    namespace App\Http\Controllers;

    class Mobil extends Controller
    {
        public function manage()
        {
            return view('mobil.manage');
        }

        public function rent()
        {
            return view('mobil.rent');
        }

        public function return()
        {
            return view('mobil.return');
        }
    }
