$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function(){
    var cek = 1;
    var pd = $('#pd').val()
    var p = pd.substr(0,2)
    var d = pd.substr(2,2)
    var setup = setInterval(() => {
        if (cek == 1) {
            $('#propinsi_filter').val(p).change();
            cek = 0;
        }else {
            if ($('#propinsi_filter').val() == p && $('#dati_filter option').length > 1) {
                $('#dati_filter').val(d).change();
                if ($('#dati_filter option').length > 0) {
                    clearInterval(setup)
                }
            }
        }
    }, 500);
})
function toast(data) {
    if (data != null){
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

        Toast.fire({
            icon: data.tipe,
            title: data.status
        })
    }
}

/* Fungsi formatRupiah */
function formatRupiah(angka, prefix){
    var number_string = angka.toString(),
    split   		= number_string.split(','),
    sisa     		= split[0].length % 3,
    rupiah     		= split[0].substr(0, sisa),
    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? ' ' + rupiah : '');
}

$("input[name='nilai_baru[]']").attr('maxlength','12').on('input', function(e) {
    $(this).val($(this).val().replace(/[^0-9]/g, ''));
});

$('#thn').attr('maxlength','4').on('input', function(e) {
    $(this).val($(this).val().replace(/[^0-9]/g, ''));
}).datepicker({ 
    format: 'yyyy',
    viewMode: "years", 
    minViewMode: "years",
    autoclose: true,
}).on('changeDate', function(){
    $('.datepicker').hide();
}).datepicker('update', new Date());

$('#thn').on('changeDate', function(ev){
    $(this).datepicker('hide');
});

$('#thn').attr('autocomplete', 'false')

$('#terbitBuku1').on("keyup change",function(e){
    var tanggal = $(this).val()
    $('#terbitBuku2').val(tanggal)
    $('#terbitBuku3').val(tanggal)
    $('#terbitBuku4').val(tanggal)
    $('#terbitBuku5').val(tanggal)
})

$('#tempoBuku1').on("keyup change",function(e){
    var tanggal = $(this).val()
    $('#tempoBuku2').val(tanggal)
    $('#tempoBuku3').val(tanggal)
    $('#tempoBuku4').val(tanggal)
    $('#tempoBuku5').val(tanggal)
})

$('#btnRefresh').click(function() {
    location.reload();
});
function cekDatiKecKelBlok(){
    var dati_filter = $('#dati_filter').length != 0
    var kecamatan_filter = $('#kecamatan_filter').length != 0
    var kelurahan_filter = $('#kelurahan_filter').length != 0
    var blok_filter = $('#blok_filter').length != 0

    return {dati_filter, kecamatan_filter, kelurahan_filter, blok_filter}
}
$(document).on('change', "#propinsi_filter", function(){
    var cek = cekDatiKecKelBlok();
    if (cek.dati_filter) {
        var textDati = $('#dati_filter').find('option:first').attr('selected', true).text()
        $('#select2-dati_filter-container').text(textDati).attr('title',textDati)
        $('#dati_filter>option:first').removeAttr('disabled')
    }
    if (cek.kecamatan_filter) {
        var textKec = $('#kecamatan_filter').find('option:first').attr('selected', true).text()
        $('#select2-kecamatan_filter-container').text(textKec).attr('title',textKec)
        $('#kecamatan_filter>option:first').removeAttr('disabled')
    }
    if (cek.kelurahan_filter) {
        var textKel = $('#kelurahan_filter').find('option:first').attr('selected', true).text()
        $('#select2-kelurahan_filter-container').text(textKel).attr('title',textKel)
        $('#kelurahan_filter>option:first').removeAttr('disabled')
    }
    if (cek.blok_filter) {
        var textBlok = $('#blok_filter').find('option:first').attr('selected', true).text()
        $('#select2-blok_filter-container').text(textBlok).attr('title',textBlok)
        $('#blok_filter>option:first').removeAttr('disabled')
    }
});

$(document).on('change', "#dati_filter", function(){
    var cek = cekDatiKecKelBlok();
    if (cek.kecamatan_filter) {
        var textKec = $('#kecamatan_filter').find('option:first').attr('selected', true).text()
        $('#select2-kecamatan_filter-container').text(textKec).attr('title',textKec)
        $('#kecamatan_filter>option:first').removeAttr('disabled')
    }
    if (cek.kelurahan_filter) {
        var textKel = $('#kelurahan_filter').find('option:first').attr('selected', true).text()
        $('#select2-kelurahan_filter-container').text(textKel).attr('title',textKel)
        $('#kelurahan_filter>option:first').removeAttr('disabled')
    }
    if (cek.blok_filter) {
        var textBlok = $('#blok_filter').find('option:first').attr('selected', true).text()
        $('#select2-blok_filter-container').text(textBlok).attr('title',textBlok)
        $('#blok_filter>option:first').removeAttr('disabled')
    }
});
$(document).on('change', "#kecamatan_filter", function(){
    var cek = cekDatiKecKelBlok();
    if (cek.kelurahan_filter) {
        var textKel = $('#kelurahan_filter').find('option:first').attr('selected', true).text()
        $('#select2-kelurahan_filter-container').text(textKel).attr('title',textKel)
        $('#kelurahan_filter>option:first').removeAttr('disabled')
    }
    if (cek.blok_filter) {
        var textBlok = $('#blok_filter').find('option:first').attr('selected', true).text()
        $('#select2-blok_filter-container').text(textBlok).attr('title',textBlok)
        $('#blok_filter>option:first').removeAttr('disabled')
    }
});

$(document).on('change', "#kelurahan_filter", function(){
    var cek = cekDatiKecKelBlok();
    if (cek.blok_filter) {
        var textBlok = $('#blok_filter').find('option:first').attr('selected', true).text()
        $('#select2-blok_filter-container').text(textBlok).attr('title',textBlok)
        $('#blok_filter>option:first').removeAttr('disabled')
    }
});