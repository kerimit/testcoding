<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Login;
use App\Http\Controllers\Register;
use App\Http\Controllers\Mobil;

Route::get('/', function () {
    return view('landing.index');
});

Route::get('/login', [Login::class, 'index'])->middleware('guest');
Route::post('/login', [Login::class, 'authenticate'])->name('login');
Route::get('/register', [Register::class, 'index'])->middleware('guest');
Route::post('/register', [Register::class, 'register'])->name('register');
Route::post('/logout', [Login::class, 'logout'])->name('logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('dashboard', function () {
        return view('dashboard.index');
    })->name('home');
    Route::prefix('mobil')->group(function () {
        Route::get('manage', [Mobil::class, 'manage']);
        Route::get('rent', [Mobil::class, 'rent']);
        Route::get('return', [Mobil::class, 'return']);
    });
});
