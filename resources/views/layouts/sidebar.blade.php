<aside class="main-sidebar sidebar-dark-gray elevation-3">
    <!-- Brand Logo -->
    <a href="{{ url('dashboard') }}" class="brand-link">
        <div class="mt-2">
            <span class="brand-text">MY</span><span class="font-weight-bold">RENTAL</span>
        </div>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu"
                data-accordion="false">
                <li class="nav-item">
                    <a href="{{ url('dashboard') }}" class="nav-link">
                        <p class=" ">Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('mobil/manage') }}" class="nav-link">
                        <p>Manajemen Mobil</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('mobil/rent') }}" class="nav-link">
                        <p>Peminjaman Mobil</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('mobil/return') }}" class="nav-link">
                        <p class>Pengembalian Mobil</p>
                    </a>
                </li>

            </ul>
        </nav>

        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
