<style type="text/css">
    .border_atas {
        border-top: 1px solid black;
        border-collapse: collapse;
    }

    .border_kiri {
        border-left: 1px solid black;
        border-collapse: collapse;
    }

    .border_kanan {
        border-right: 1px solid black;
        border-collapse: collapse;
    }

    .border_bawah {
        border-bottom: 1px solid black;
        border-collapse: collapse;
    }

    .border_bawah_tebal {
        border-bottom: 2px solid black;
        border-collapse: collapse;
    }

    .font_lima {
        font-size: 5pt;
    }

    .font_tujuh {
        font-size: 7pt;
    }

    .font_delapan {
        font-size: 8pt;
    }

    .font_sepuluh {
        font-size: 9pt;
    }

    .font_sembilan {
        font-size: 9pt;
    }

    .font_sepuluh {
        font-size: 10pt;
    }

    .font_sebelas {
        font-size: 11pt;
    }
    

    .font_duabelas {
        font-size: 12pt;
    }

    .kolom {
        display: inline-table !important;
    }

    .font_bold {
        font-weight: bold;
    }

    .text_tengah {
        text-align: center;
    }

    .text_kanan {
        text-align: right;
    }

    table {
        font-family: Arial;
    }

    td {
        padding-right: 5px;
        padding-left: 5px;
    }

    .pad-5 {
        padding-right: 5px;
        padding-left: 5px;
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .pad-10 {
        padding-right: 10px;
        padding-left: 10px;
        padding-top: 10px;
        padding-bottom: 10px;
    }

    table {
        border-collapse: collapse;
    }

    th {
        border: 1px solid black;
        margin: 0;
        padding: 10px;
    }

    .div-table {
        display: table;
        width: auto;
        border-spacing: 5px;
        /* cellspacing:poor IE support for  this */
    }

    .div-table-row {
        display: table-row;
        width: auto;
        clear: both;
    }

    .div-table-col {
        float: left;
        /* fix for  buggy browsers */
        display: table-column;
        min-width: 200px;
        /* background-color: #ccc; */
    }

    @page {
        margin: 40px;
        margin-top: 30px;
        margin-bottom: 70px;
        footer: page-footer;
        header: page-header;
    }
</style>