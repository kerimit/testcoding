<nav class="main-header navbar navbar-expand navbar-white navbar-light border-bottom-0 elevation-3">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item p-1">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>

    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        {{-- <li class="nav-item dropdown p-1">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge">15</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header text-left font-weight-bold text-md">Notifikasi</span>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-envelope mr-2"></i> 4 new messages
                    <span class="float-right text-muted text-sm">3 mins</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-users mr-2"></i> 8 friend requests
                    <span class="float-right text-muted text-sm">12 hours</span>
                </a>
                <div class="p-2 text-center">
                    <img src="{{ asset('dist/img/others/5.png') }}" alt="" class="w-50" >
                    <p class="text-muted text-sm text-center mt-2">Belum Ada Notifikasi</p>
                </div>


                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item dropdown-footer text-primary text-small">Lihat Semua Notifikasi</a>
            </div>
        </li> --}}

        <li class="nav-item p-1">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>

        <li class="nav-item dropdown">
            {{-- <a class="nav-link" data-toggle="dropdown" href="#"></a> --}}
            <div class="btn btn-sm w-auto btn-clean d-flex align-items-center px-2" data-toggle="dropdown">
                <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hai,</span>
                <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{Auth::user()->nama}}</span>
                <img src="{{ asset('dist/img/user1-128x128.jpg') }}" class="img-rounded elevation-3" style="height: 40px;"
                    alt="User Image">
            </div>
            <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right">
                <div class="dropdown-item elevation-3">
                    <div class="d-flex justify-content-start py-2">
                        <img src="{{ asset('dist/img/user1-128x128.jpg') }}" class="img-rounded elevation-3" style="height: 90px;"
                        alt="User Image">
                        <div class="px-3">
                            <h5>{{Auth::user()->nama}}</h5>
                            <h6 class="text-muted text-sm">Admin</h6>
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <button class="btn btn-sm btn-outline-danger">Logout</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </li>

    </ul>
</nav>
