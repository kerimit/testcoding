@php
    use Illuminate\Support\Facades\Auth;
@endphp

@extends('layouts.app')

@section('leftTitle')
<h1 class="m-0">Dashboard</h1>
<h5 class="mb-2 text-dark-50">Selamat datang, {{ Auth::user()->name }}<span class="text-dark-75"></span></h5>
@endsection
