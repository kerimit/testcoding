@extends('layouts.app')

@section('leftTitle')
    <h1 class="m-0">Manajemen Mobil</h1>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                                Tambah
                            </button>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="manajemen-mobil">
                                    <thead class="bg-primary">
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Kendaraan</th>
                                            <th>Merek</th>
                                            <th>No. Plat</th>
                                            <th>Jenis</th>
                                            <th>Bahan Bakar</th>
                                            <th>Jml Pintu</th>
                                            <th>Jml Kursi</th>
                                            <th>AT/MT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
