<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.meta')

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">

    <link rel="stylesheet" href="{{ asset('dist/css/animate.css') }}">

    <!-- Custom style -->
    <link rel="stylesheet" href="{{ asset('dist/css/custom.css') }}">

</head>

<body class="hold-transition">
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 p-5 p-sm-4 p-xl-5 p-lg-5 login-section-wrapper animated slideInLeft">
                <div class="login-wrapper my-auto">
                    <h3 class="login-title">Registrasi</h3>
                    <p class=""><span class="text-primary">Masukan Informasi </span> data diri anda</p>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form action="register" method="post">
                    @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" name="username" id="username" class="form-control @error('username') is-invalid @enderror" placeholder="Masukan Username" value="{{ old('username') }}">
                                    @error('username')
                                        <span style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror"  placeholder="Masukan Password" value="{{ old('password') }}">
                                    @error('password')
                                        <span style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="password">Ulangi Password</label>
                                    <input type="password" name="repassword" id="repassword" class="form-control @error('repassword') is-invalid @enderror"  placeholder="Ulangi Password" value="{{ old('repassword') }}">
                                    @error('repassword')
                                        <span style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="name">Nama Lengkap</label>
                                    <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name Lengkap" value="{{ old('name') }}">
                                    @error('name')
                                        <span style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Nomor Telepon</label>
                                    <input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}">
                                    @error('email')
                                        <span style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="telp">Nomor Telepon</label>
                                    <input type="text" name="telp" id="telp" class="form-control @error('telp') is-invalid @enderror" placeholder="Nomor Telepon" value="{{ old('telp') }}">
                                    @error('telp')
                                        <span style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="sim">Nomor SIM</label>
                                    <input type="text" name="sim" id="sim" class="form-control @error('sim') is-invalid @enderror" placeholder="Nomor SIM" value="{{ old('sim') }}">
                                    @error('sim')
                                        <span style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="address">Alamat</label>
                                    <textarea name="address" id="address" class="form-control @error('address') is-invalid @enderror" placeholder="Alamat">{{ old('address') }}</textarea>
                                    @error('address')
                                        <span style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Registrasi</button>
                    </form>
                    <div class="mt-2">
                        {{-- <a href="#!" class="forgot-password-link">Lupa password?</a> --}}
                        {{-- <p class="login-wrapper-footer-text">Tidak punya akun? <a href="#!" class="text-muted">Daftar
                            disini</a></p> --}}
                    </div>
                </div>
                {{-- <footer>
                    <strong>Copyright &copy; 2021 <a href="https://adminlte.io">SMPD</a>.</strong>
                    <div class="float-right d-none d-sm-inline-block">
                        <b>Version</b> 0.1
                    </div>
                </footer> --}}
            </div>
            <div class="col-sm-6 px-0 d-none d-sm-block">
                <div class="login-img elevation-3"
                    style="background: linear-gradient(to right, rgba(147,51,234, 0.7), rgba(206, 53, 220, 0.5)),
                url(https://images.unsplash.com/photo-1501179691627-eeaa65ea017c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80) no-repeat center center; ">
                    <div class="d-flex flex-column justify-content-end align-items-end h-auto">
                        <div class="text-center mt-md-5 mt-sm-4">
                            <div class="d-flex align-content-center ">
                                <div class="d-flex flex-column align-self-center p-3">
                                    <h3 class="font-weight-bolder text-white text-right animated fadeIn">MYRENTAL</h3>
                                    <h4 class="text-white font-weight-normal animated fadeIn text-right">Test Programming</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>



    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
</body>

</html>
