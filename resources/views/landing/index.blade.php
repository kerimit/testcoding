<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.meta')

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/swiper.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/landing/css/responsive.css') }}">
</head>

<body>
    {{-- @dump(asset('dist/landing/css/animate.min.css')) --}}
    {{-- <div class="preloader" id="preloader"><span></span></div> --}}
    <div class="page-wrapper">
        {{-- @dump(asset('plugins/fontawesome-free/css/all.min.css')) --}}
        <header class="site-header site-header__header-two ">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <div class="logo-box clearfix">
                        <a class="navbar-brand text-bold" href="index.html">
                            {{-- <img src="dist/landing/images/smpd_logo_purple.png" class="main-logo" height="40"
                                alt="Awesome Image" /> --}}
                                MYRENTAL
                        </a>
                        <button class="menu-toggler" data-target=".main-navigation">
                            <span class="fa fa-bars"></span>
                        </button>
                    </div>

                    <div class="main-navigation">
                        <ul class="one-page-scroll-menu navigation-box">
                            <li class="current scrollToLink">
                                <a href="#halaman-utama">Halaman Utama</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="#tentang">Tentang</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="#fitur">Fitur</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="#pembayaran">Pembayaran</a>
                            </li>
                            {{-- <li class="scrollToLink">
                                <a href="#alur">Alur</a>
                            </li> --}}
                            <li class="scrollToLink">
                                <a href="#faq">Faq</a>
                            </li>
                        </ul>
                    </div>
                    <div class="right-side-box">
                        @auth
                        <a href="{{ route('home') }}" class="btn btn-light btn-lg elevation-3 text-primary">Home</a>
                        @endauth
                        @guest
                        <a href="{{ url('login') }}" class="btn btn-light btn-lg elevation-3 text-primary">Sign in</a>
                        <a href="{{ url('register') }}" class="btn btn-success btn-lg elevation-3 text-primary">Registrasi</a>
                        @endguest
                    </div>
                </div>

            </nav>
        </header>
        <section class="section-one" id="halaman-utama">
            <div class="container">
                <div class="section-one__moc">
                    <img src="dist/landing/images/11.png" alt="">
                </div>
                <div class="row">
                    <div class="col-xl-6 col-lg-8">
                        <div class="section-one__content">
                            <img src="dist/landing/images/blobs-2.png" style="height: 60%;" class="section-one__line"
                                alt="">
                            <h3 class="section-one__title">Sistem Manajemen <br> Sewa Mobil</h3>
                            <p class="section-one__text">Aplikasi yang digunakan untuk memanajemen Sewa Mobil.
                            </p>
                            <a href="{{ url('login') }}" class="thm-btn cta-one__btn">Masuk ke Aplikasi</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="brand-one">
                <div class="container">
                    <div class="brand-one__carousel owl-carousel owl-theme">
                        <div class="item">
                            <img src="dist/landing/images/qris_white.png" alt="">
                        </div>
                        <div class="item">
                            <img src="https://gigg.id/img/gopay.png" alt="">
                        </div>
                        <div class="item">
                            <img src="https://1.bp.blogspot.com/-Iq0Ztu117_8/XzNYaM4ABdI/AAAAAAAAHA0/MabT7B02ErIzty8g26JvnC6cPeBZtATNgCLcBGAsYHQ/s1000/logo-ovo.png" alt="">
                        </div>
                        <div class="item">
                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Logo_dana_blue.svg/1200px-Logo_dana_blue.svg.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-two" id="tentang">
            <div class="container">
                <div class="section-two__image">
                    <img src="dist/landing/images/blobs-1.png" width="50%" class="section-two__line" alt="">
                    <img src="dist/landing/images/7.png" alt="" height="400px;">
                </div>
                <div class="row">
                    <div class="col-xl-6 col-lg-7">
                        <div class="section-two__content">
                            <div class="block-title text-left">
                                <p class="block-title__tag-line">Tentang Aplikasi
                                </p>
                                <h2 class="block-title__title">Penjelasan Singkat <br>
                                    Mengenai MYRENTAL <br></h2>
                            </div>
                            <p class="section-two__text">Ini hanya untuk test ajah</div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-three thm-gray-bg" id="fitur">
            <div class="container">
                <div class="block-title text-center">
                    <p class="block-title__tag-line">Fitur-fitur Aplikasi
                    </p>
                    <h2 class="block-title__title">Keunggulan Aplikasi MYRENTAL</h2>
                </div>
                <div class="service-one__content-wrap mb-lg-5">
                    <div class="service-one__single">
                        <div class="service-one__inner">
                            <i class="fas fa-shapes fa-2x"></i>
                            <h3 class="service-one__title"><a href="#">Aplikasi<br>Responsif</a></h3>
                            <div class="mt-4 p-2">
                                <span>Aplikasi bisa dibuka dengan berbagai macam device.</span>
                            </div>
                        </div>
                    </div>
                    <div class="service-one__single">
                        <div class="service-one__inner">
                            <i class="fas fa-door-open fa-2x"></i>
                            <h3 class="service-one__title"><a href="#">Cepat<br>Diakses</a></h3>
                            <div class="mt-4 p-2">
                                <span>Aplikasi sangat cepat diakses dari mana saja.</span>
                            </div>
                        </div>
                    </div>
                    <div class="service-one__single">
                        <div class="service-one__inner">
                            <i class="fas fa-diagnoses fa-2x"></i>
                            <h3 class="service-one__title"><a href="#">Mudah<br>Digunakan</a></h3>
                            <div class="mt-4 p-2">
                                <span>Aplikasi sangat mudah digunakan oleh user.</span>
                            </div>
                        </div>
                    </div>
                    <div class="service-one__single">
                        <div class="service-one__inner">
                            <i class="fas fa-shield-alt fa-2x"></i>
                            <h3 class="service-one__title"><a href="#">Keamanan <br> Terjaga</a></h3>
                            <div class="mt-4 p-2">
                                <span>Keamanan aplikasi yang sudah terjaga dengan baik.</span>
                            </div>
                        </div>
                    </div>
                    <div class="service-one__single">
                        <div class="service-one__inner ">
                            <i class="fas fa-donate fa-2x"></i>
                            <h3 class="service-one__title"><a href="#">Pembayaran <br> Mudah</a></h3>
                            <div class="mt-4 p-2">
                                <span>Akses pembayaran yang sangat mudah.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-payments" id="pembayaran">
            <div class="container">
                <div class="block-title text-center">
                    <p class="block-title__tag-line">Metode Pembayaran
                    </p>
                    <h2 class="block-title__title">Cara Termudah Untuk Bayar</h2>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="section-payments__single color-1 wow fadeInDown elevation-3 bg-white"
                            data-wow-duration="1500ms">
                            <div class="section-payments__top">
                                <h3 class="section-payments__amount"> Bank <br> Transfer </h3>
                            </div>
                            <ul class="list-unstyled section-payments__list">
                                <li>Bank Pembangunan Daerah</li>
                                <li>BNI</li>
                                <li>BRI</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="section-payments__single color-2 wow fadeInUp elevation-3 bg-white"
                            data-wow-duration="1500ms">
                            <div class="section-payments__top">
                                <h3 class="section-payments__amount text-danger"> Online Payments </h3>
                            </div>
                            <ul class="list-unstyled section-payments__list">
                                <li>OVO</li>
                                <li>Gopay</li>
                                <li>QRIS</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="section-payments__single color-3 wow fadeInDown elevation-3 bg-white"
                            data-wow-duration="1500ms">
                            <div class="section-payments__top">
                                <h3 class="section-payments__amount"> Market Payments </h3>
                            </div>
                            <ul class="list-unstyled section-payments__list">
                                <li>Indomaret</li>
                                <li>Alfamaret</li>
                                <li>Tokopedia</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{-- <section class="section-three section-three__home thm-gray-bg" id="alur">
            <div class="container">
                <div class="block-title text-center">
                    <p class="block-title__tag-line">Alur</p>
                    <h2 class="block-title__title">Alur Aplikasi</h2>
                </div>
                <div class="timeline">
                    <ul>
                        <li>
                            <div class="content wow fadeInLeft">
                                <h3>Pendaftaran</h3>
                                <p>Lakukan pengisian form dan lengkapi persyaratan sesuai dengan data anda yang benar.
                                </p>
                            </div>
                            <div class="time wow fadeInRight">
                                <h4>Langkah 1</h4>
                            </div>
                        </li>

                        <li>
                            <div class="content wow fadeInRight">
                                <h3>Validasi Berkas</h3>
                                <p>Formulir pendaftaran dan kelengkapan syarat pendaftaran akan dicek oleh petugas
                                    validasi
                                    berkas</p>
                            </div>
                            <div class="time wow fadeInLeft">
                                <h4>Langkah 2</h4>
                            </div>
                        </li>

                        <li>
                            <div class="content wow fadeInLeft">
                                <h3>Validasi admin/h3>
                                <p>Setelah kebenaran formulir pendaftaran dan syarat pendaftaran telah lengkap,
                                    selanjutnya
                                    tahap proses validasi oleh kepala bidang.</p>
                            </div>
                            <div class="time wow fadeInRight">
                                <h4>Langkah 3</h4>
                            </div>
                        </li>

                        <li>
                            <div class="content wow fadeInRight">
                                <h3>Pembayaran</h3>
                                <p>Setelah semua proses admin sudah terpenuhi, maka bisa dilakukan pembayaran
                                    ketempat
                                    pemayaran yang ditunjukan. </p>
                            </div>
                            <div class="time wow fadeInLeft">
                                <h4>Langkah 4</h4>
                            </div>
                        </li>

                        <div style="clear:both;"></div>
                    </ul>
                </div>
            </div>
        </section> --}}

        <section class="faq" id="faq">
            <div class="container">
                <div class="block-title text-center">
                    <p class="block-title__tag-line">FAQ
                    </p>
                    <h2 class="block-title__title">Pertanyaan Yang Sering Ditanyakan</h2>
                </div>
                <ul class="faq-list">
                    <li data-aos="fade-up" data-aos-delay="100" class="aos-init aos-animate wow fadeInUp">
                        <a data-toggle="collapse" class="collapsed" href="#faq1" aria-expanded="false">Apa saja mobil yang bisa disewa?
                            <i class="fas fa-arrow-up"></i></a>
                        <div id="faq1" class="collapse" data-parent=".faq-list" style="">
                            <p>
                                Apa saja ada, dari mobil LGCC, sampai dengan mobil mewah seperti Corvete, Lamborghini dan ESEMKA
                            </p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>

        <section class="section-thanks">
            <div class="container text-center">
                <h3 class="block-title__title">Terimakasih Anda Telah Mengakses<br>
                    Aplikasi MYRENTAL</h3><!-- /.block-title__title -->
                <p class="cta-one__text">anda adalah orang bijak karena tidak beli mobil, mending sewa ajah.</p>
                <div class="section-thanks__image">
                    <img class="section-thanks__image" src="dist/landing/images/thanks.png" height="250px" alt="">
                </div>

            </div><!-- /.container -->
        </section><!-- /.cta-one -->
        <footer class="site-footer">
            <div class="site-footer__bubble-1"></div><!-- /.site-footer__bubble-1 -->
            <div class="site-footer__line"></div><!-- /.site-footer__line -->
            <div class="site-footer__bubble-2"></div><!-- /.site-footer__bubble-2 -->
            <div class="site-footer__upper">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="footer-widget footer-widget__about">
                                <p class="footer-widget__contact"><a href="tel:888-666-0000">-</a></p>
                                <!-- /.footer-widget__contact -->
                                <p class="footer-widget__contact"><a
                                        href="mailto:needhelp@example.com"com@com.com</a>
                                </p><!-- /.footer-widget__contact -->
                            </div><!-- /.footer-widget -->
                        </div><!-- /.col-lg-2 -->
                        <div class="col-lg-6 d-flex justify-content-between footer-widget__links-wrap">
                            <div class="footer-widget">
                                <h3 class="footer-widget__title">Informasi</h3><!-- /.footer-widget__title -->
                                <ul class="footer-widget__links list-unstyled">
                                    <li><a href="#">-</a></li>
                                    <li><a href="#">-</a></li>
                                </ul><!-- /.footer-widget__links -->
                            </div><!-- /.footer-widget -->

                            <div class="footer-widget">
                                <h3 class="footer-widget__title">Tautan</h3><!-- /.footer-widget__title -->
                                <ul class="footer-widget__links list-unstyled">
                                    <li><a href="#">-</a></li>
                                    <li><a href="#">-</a></li>
                                </ul><!-- /.footer-widget__links -->
                            </div><!-- /.footer-widget -->
                        </div><!-- /.col-lg-6 -->
                        <div class="col-lg-4">
                            <div class="footer-widget footer-widget__mailchimp">
                                <h3 class="footer-widget__title">Kontak</h3>
                                <ul class="footer-widget__links list-unstyled">
                                    <li><a href="#">
                                        -
                                    </a></li>
                                    <li><a href="">-</a></li>
                                </ul><!-- /.footer-widget__links --><!-- /.footer-widget__title -->
                                <div class="card mt-3">
                                    <div class="card-body p-0">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d63452.67766137905!2d106.7588497!3d-6.29099995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sid!2sid!4v1619753167484!5m2!1sid!2sid" width="100%" height="100" style="border:0; border-radius: 1rem;" allowfullscreen="" loading="lazy"></iframe>
                                    </div>
                                </div>
                                <div class="mc-form__response"></div><!-- /.mc-form__response -->
                            </div><!-- /.footer-widget -->
                        </div><!-- /.col-lg-4 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.site-footer__upper -->
            <div class="site-footer__bottom">
                <div class="container">
                    <div class="inner-container">
                        <p class="site-footer__copy">&copy; copyright 2023 by Anonymouus</a></p>
                    </div><!-- /.inner-container -->
                </div><!-- /.container -->
            </div><!-- /.site-footer__bottom -->
        </footer><!-- /.site-footer -->
    </div><!-- /.page-wrapper -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <script src="{{ asset('dist/landing/js/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/TweenMax.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/wow.js') }}"></script>
    <script src="{{ asset('dist/landing/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/swiper.min.js') }}"></script>
    <script src="{{ asset('dist/landing/js/jquery.easing.min.js') }}"></script>

    <script src="{{ asset('dist/landing/js/theme.js') }}"></script>

</body>

</html>
